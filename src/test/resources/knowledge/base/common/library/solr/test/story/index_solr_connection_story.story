Scenario:  Index a document successfully
Given a solr connection of https://xxx:123/solr/124
When it request to add a document with fields:
|col1|col2|col3|
|dummayval|abcdef|123val|
Then document should be added

Scenario: Index a document but failed upon calling SolrClient.add
Given a solr connection of https://xxx:123/solr/124
When it request to add a document but failed at SolrClient.add with fields:
|col1|col2|col3|
|dummayval|abcdef|123val|
Then rollback was imposed

Scenario:  Index a list of less 100 document successfully
Given a solr connection of https://xxx:123/solr/124
When it request to add a list of document with fields:
|col1|col2|col3|
|dummayval|abcdef|123val|
|dummayval1|abcdef1|123val1|
|dummayval2|bcdef1|23val1|
|dummayval3|cdef1|3val1|
Then list of document should be added by 1 batch/es

Scenario:  Index a list of 101 document successfully
Given a solr connection of https://xxx:123/solr/124
!--Create Dummy data for 101 times
When it request to add a list of 101 document with fields:
|col1|col2|col3|
|x|y|z|
Then list of document should be added by 2 batch/es

Scenario:  Index a list of 300 document successfully
Given a solr connection of https://xxx:123/solr/124
!--Create Dummy data for 300 times
When it request to add a list of 300 document with fields:
|col1|col2|col3|
|x|y|z|
Then list of document should be added by 3 batch/es


