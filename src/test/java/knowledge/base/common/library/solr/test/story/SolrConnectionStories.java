package knowledge.base.common.library.solr.test.story;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.jbehave.core.Embeddable;
import org.jbehave.core.configuration.Configuration;
import org.jbehave.core.configuration.MostUsefulConfiguration;
import org.jbehave.core.embedder.StoryControls;
import org.jbehave.core.i18n.LocalizedKeywords;
import org.jbehave.core.io.CodeLocations;
import org.jbehave.core.io.LoadFromClasspath;
import org.jbehave.core.io.StoryFinder;
import org.jbehave.core.io.UnderscoredCamelCaseResolver;
import org.jbehave.core.junit.JUnitStories;
import org.jbehave.core.model.ExamplesTableFactory;
import org.jbehave.core.model.TableTransformers;
import org.jbehave.core.parsers.RegexStoryParser;
import org.jbehave.core.reporters.CrossReference;
import org.jbehave.core.reporters.FilePrintStreamFactory;
import org.jbehave.core.reporters.Format;
import org.jbehave.core.reporters.StoryReporterBuilder;
import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.jbehave.core.steps.ParameterConverters;
import org.junit.runner.RunWith;

import com.github.valfirst.jbehave.junit.monitoring.JUnitReportingRunner;

import knowledge.base.common.library.solr.test.SolrConnectionSteps;

@RunWith(JUnitReportingRunner.class)
public class SolrConnectionStories extends JUnitStories {
	private final CrossReference xref = new CrossReference();

	public SolrConnectionStories() {
        // configure as TraderStory except for 
		configuredEmbedder().embedderControls().doGenerateViewAfterStories(true).doIgnoreFailureInStories(true)
        .doIgnoreFailureInView(true).useThreads(1).useStoryTimeoutInSecs(60);

    }

	 @Override
	    public Configuration configuration() {
	        Class<? extends Embeddable> embeddableClass = this.getClass();
	        Properties viewResources = new Properties();
	        viewResources.put("decorateNonHtml", "true");
	        // Start from default ParameterConverters instance
	        ParameterConverters parameterConverters = new ParameterConverters();
	        // factory to allow parameter conversion and loading from external
	        // resources (used by StoryParser too)
	        ExamplesTableFactory examplesTableFactory = new ExamplesTableFactory(new LocalizedKeywords(),
	                new LoadFromClasspath(embeddableClass), parameterConverters, new TableTransformers() );
	        // add custom converters
	        parameterConverters.addConverters(new ParameterConverters.DateConverter(new SimpleDateFormat("yyyy-MM-dd")),
	                new ParameterConverters.ExamplesTableConverter(examplesTableFactory));
	 
	        return new MostUsefulConfiguration()
	                .useStoryControls(new StoryControls().doDryRun(false).doSkipScenariosAfterFailure(false))
	                .useStoryLoader(new LoadFromClasspath(embeddableClass))
	                .useStoryParser(new RegexStoryParser(examplesTableFactory))
	                .useStoryPathResolver(new UnderscoredCamelCaseResolver())
	                .useStoryReporterBuilder(
	                        new StoryReporterBuilder()
	                                .withCodeLocation(CodeLocations.codeLocationFromClass(embeddableClass))
	                                .withDefaultFormats().withPathResolver(new FilePrintStreamFactory.ResolveToPackagedName())
	                                .withViewResources(viewResources).withFormats( Format.CONSOLE, Format.TXT, Format.HTML, Format.XML)
	                                .withFailureTrace(true).withFailureTraceCompression(true).withCrossReference(xref))
	                .useParameterConverters(parameterConverters)
	                // use '%' instead of '$' to identify parameters
	                //.useStepPatternParser(new RegexPrefixCapturingPatternParser("%")) 
	                .useStepMonitor(xref.getStepMonitor());
	    }
	 
	 
	@Override
	protected List<String> storyPaths() {
		String codeLocation = CodeLocations.codeLocationFromClass(this.getClass()).getFile();
        return new StoryFinder().findPaths(codeLocation, Arrays.asList("**/*.story"), Arrays.asList(""), "");
	}

	 @Override
	    public InjectableStepsFactory stepsFactory() {
	        return new InstanceStepsFactory(configuration(), new SolrConnectionSteps());
	    }
}


