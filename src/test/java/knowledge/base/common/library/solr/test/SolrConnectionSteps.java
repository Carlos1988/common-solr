package knowledge.base.common.library.solr.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

import org.apache.commons.collections4.map.HashedMap;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrInputDocument;
import org.jbehave.core.annotations.AfterScenario;
import org.jbehave.core.annotations.BeforeScenario;
import org.jbehave.core.annotations.BeforeStories;
import org.jbehave.core.annotations.BeforeStory;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.model.ExamplesTable;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import com.thoughtworks.xstream.io.xml.Dom4JXmlWriter;

import knowledge.base.common.library.solr.ISolrDocument;
import knowledge.base.common.library.solr.SolrCommonDocument;
import knowledge.base.common.library.solr.SolrConnection;
import knowledge.base.common.library.solr.SolrConnectionFactory;
import knowledge.base.common.library.solr.SolrConnectionParameters;

public class SolrConnectionSteps {
	@Mock
	private HttpSolrClient.Builder mockBuilder = Mockito.mock(HttpSolrClient.Builder.class);
	@Mock
	private HttpSolrClient mockSolrClient = Mockito.mock(HttpSolrClient.class);
	@InjectMocks
	private SolrConnection connection = new SolrConnection(this.mockBuilder);
	SolrConnectionParameters params;
	
	@BeforeStory
	@BeforeScenario
	public void setUpScenario() {
		//clear all stubs
		Mockito.reset(this.mockBuilder, this.mockSolrClient);
	}

	@Given("a solr connection of https://$domain:$port/solr/$document")
	public void aSolrConnectionOf(String domain, String port, String document) throws Exception {
		ISolrDocument doc = new SolrCommonDocument("MyDocument");
		this.params = new SolrConnectionParameters(true, "mysomething", "1111", doc);
		SolrConnection cconcrete_onnection = SolrConnectionFactory.getConnection(params);
		assertNotNull("SolrConnection object should be instantiated", cconcrete_onnection);
		assertEquals("https://mysomething:1111/solr/MyDocument",this.params.getUrl());
	}
	
	@When("it request to add a document with fields: $documentsTable")
	public void itReqToAddDocSuccessfully(ExamplesTable documentsTable) {
		String message = "Add document with fields " + documentsTable.toString();
		try {
			
			//set-up
			Mockito.when(this.mockBuilder.build()).thenReturn(this.mockSolrClient);
			Mockito.when(this.mockSolrClient.add(Mockito.any(SolrInputDocument.class)))
			.thenReturn(Mockito.mock(UpdateResponse.class));
			
			assertTrue(message, this.connection.index(documentsTable.getRow(0)));
		} catch (SolrServerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail(message);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail(message);
		}
		
	}
	
	
	@When("it request to add a list of document with fields: $documentsTable")
	public void itReqToAddAListOfDocsSuccessfully(ExamplesTable documentsTable) {
		String message = "Add document with fields " + documentsTable.toString();
		try {
			
			//set-up
			Mockito.when(this.mockBuilder.build()).thenReturn(this.mockSolrClient);
			Mockito.when(this.mockSolrClient.add(Mockito.any(Collection.class)))
			.thenReturn(Mockito.mock(UpdateResponse.class));
			
			assertTrue(message, this.connection.indexCollections(documentsTable.getRows()));
		} catch (SolrServerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail(message);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail(message);
		}
		
	}
	
	@When("it request to add a list of ${listLength} document with fields: $documentsTable")
	public void itReqToAddAListOfNumDocsSuccessfully(int listLength, ExamplesTable documentsTable) {
		String message = "Add document with fields " + documentsTable.toString();
		try {
			
			//set-up
			Mockito.when(this.mockBuilder.build()).thenReturn(this.mockSolrClient);
			Mockito.when(this.mockSolrClient.add(Mockito.any(Collection.class)))
			.thenReturn(Mockito.mock(UpdateResponse.class));
			
			Collection<Map<String, String>> rows = new ArrayList<>();
			Map<String, String> row = documentsTable.getRow(0);
			
			for(int i = 0; i < listLength; i++) {
				Map<String, String> newRow = new HashedMap<>();
				for(Map.Entry<String, String> entry : row.entrySet()) {
					newRow.put(entry.getKey(), entry.getValue() + new Date().getTime());
				}
				rows.add(newRow);
			}
			
			assertTrue(message, this.connection.indexCollections(rows));
		} catch (SolrServerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail(message);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail(message);
		}
		
	}
	
	
	
	@When("it request to add a document but failed at SolrClient.${action} with fields: $documentsTable")
	public void itReqToAddDocFailedAtAdd(String action, ExamplesTable documentsTable) {
		String message = "Add document with fields " + documentsTable.toString();
		try {
			
			//set-up
			Mockito.when(this.mockBuilder.build()).thenReturn(this.mockSolrClient);
			
			if (action.equals("add")) {
				Mockito.when(this.mockSolrClient.add(Mockito.any(SolrInputDocument.class)))
				.thenThrow(Mockito.mock(SolrServerException.class));
			} else if (action.equals("commit")) {
				Mockito.when(this.mockSolrClient.commit())
				.thenThrow(Mockito.mock(SolrServerException.class));
			}

			assertFalse(message, this.connection.index(documentsTable.getRow(0)));
			
			
			Mockito.verify(this.mockSolrClient, Mockito.times(1)).add(Mockito.any(SolrInputDocument.class));

			if (action.equals("commit")) {
				Mockito.verify(this.mockSolrClient, Mockito.times(1)).commit();
			}
		} catch (SolrServerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail(message);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail(message);
		}
		
	}
	
	@Then("rollback was imposed")
	public void rollbackWasImposed() throws IOException, SolrServerException {
			Mockito.verify(this.mockSolrClient, Mockito.times(1)).rollback();
			Mockito.verify(this.mockSolrClient, Mockito.times(1)).close();
	}
	
	
	@Then("document should be added")
	public void addADocumentSuccessful() throws SolrServerException, IOException {
		Mockito.verify(this.mockSolrClient, Mockito.times(1)).add(Mockito.any(SolrInputDocument.class));
		Mockito.verify(this.mockSolrClient, Mockito.times(1)).commit();
		Mockito.verify(this.mockSolrClient, Mockito.times(1)).close();
	}
	
	@Then("list of document should be added by ${times} batch/es")
	public void addAListOfDocumentSuccessful(int times) throws SolrServerException, IOException {
		Mockito.verify(this.mockSolrClient, Mockito.times(times)).add(Mockito.any(Collection.class));
		Mockito.verify(this.mockSolrClient, Mockito.times(times)).commit();
		Mockito.verify(this.mockSolrClient, Mockito.times(1)).close();
	}
}
