package knowledge.base.common.library.solr;

public class SolrCommonDocument implements ISolrDocument {
	private String name;
	
	public SolrCommonDocument(String name) {
		this.name = name;
	}
	
	@Override
	public String getName() {
		return this.name;
	}

}
