package knowledge.base.common.library.solr;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;

public class SolrConnection {
	private HttpSolrClient.Builder builder;
	
	public SolrConnection(HttpSolrClient.Builder builder){
		this.builder = builder;
	}
	
	private static SolrInputDocument rawMapToDocument(Map<String, String> fields) {
		SolrInputDocument doc = new SolrInputDocument();
		for(Map.Entry<String, String> field : fields.entrySet()) {
			doc.addField(field.getKey(), field.getValue());
		}
		return doc;
	}
	
	private static Collection<SolrInputDocument> rawCollectionsToDocuments(Collection<Map<String, String>> collections){
		Collection<SolrInputDocument> docs = new ArrayList<SolrInputDocument>();
		for(Map<String, String> fields : collections) {
			docs.add(rawMapToDocument(fields));
		}
		return docs;
	}

	private static SolrInputDocument anyRawMapToDocument(Map<String, Object> fields) {
		SolrInputDocument doc = new SolrInputDocument();
		for(Map.Entry<String, Object> field : fields.entrySet()) {
			doc.addField(field.getKey(), field.getValue());
		}
		return doc;
	}
	
	private static Collection<SolrInputDocument> anyRawCollectionsToDocuments(Collection<Map<String, Object>> collections){
		Collection<SolrInputDocument> docs = new ArrayList<SolrInputDocument>();
		for(Map<String, Object> fields : collections) {
			docs.add(anyRawMapToDocument(fields));
		}
		return docs;
	}
	/**
	 * Add an entry to the document collection of Solr.
	 * @param fields The fields of the document to be saved.
	 * @return true if success else false.
	 * @throws SolrServerException Thrown when SolrConnection fail to close properly.
	 * @throws IOException Thrown when SolrConnection fail to roll back properly.
	 */
	public boolean index(Map<String, String> fields) throws SolrServerException, IOException {
		SolrClient instance = null;
		try {
			instance = this.builder.build();
			SolrInputDocument doc = rawMapToDocument(fields);
			instance.add(doc);
		    instance.commit();
			return true;
		} catch (SolrServerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			instance.rollback();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			instance.rollback();
		} finally {
			if (null != instance) {
				instance.close();
			}
		}
		return false;
	}

	/**
	 * Add an entry to the document collection of Solr.
	 * @param fields The fields of the document to be saved.
	 * @return true if success else false.
	 * @throws SolrServerException Thrown when SolrConnection fail to close properly.
	 * @throws IOException Thrown when SolrConnection fail to roll back properly.
	 */
	public boolean indexAny(Map<String, Object> fields) throws SolrServerException, IOException {
		SolrClient instance = null;
		try {
			instance = this.builder.build();
			SolrInputDocument doc = anyRawMapToDocument(fields);
			instance.add(doc);
		    instance.commit();
			return true;
		} catch (SolrServerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			instance.rollback();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			instance.rollback();
		} finally {
			if (null != instance) {
				instance.close();
			}
		}
		return false;
	}
	
	/**
	 * Add entries to the document collection of Solr.
	 * @param collections The documents to be saved.
	 * @return true is success else false.
	 * @throws IOException Thrown when SolrConnection fail to roll back properly.
	 * @throws SolrServerException Thrown when SolrConnection fail to close properly.
	 */
	public boolean indexCollections(Collection<Map<String, String>> collections) throws IOException, SolrServerException {
		SolrClient instance = null;
		try {
			instance = builder.build();
			Collection<SolrInputDocument> docs = rawCollectionsToDocuments(collections);
			while(!docs.isEmpty()) {
				Stream<SolrInputDocument> docsStream = docs.stream();
				Collection<SolrInputDocument> partCollections = docsStream.limit(100).collect(Collectors.toCollection(ArrayList::new));
				instance.add(partCollections);
				instance.commit();
				docs.removeAll(partCollections);
			}
			return true;
		} catch (SolrServerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			instance.rollback();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			instance.rollback();
		} finally {
			if (null != instance) {
				instance.close();
			}
		}
		return false;
	}

	/**
	 * Add entries to the document collection of Solr.
	 * @param collections The documents to be saved.
	 * @return true is success else false.
	 * @throws IOException Thrown when SolrConnection fail to roll back properly.
	 * @throws SolrServerException Thrown when SolrConnection fail to close properly.
	 */
	public boolean indexAnyCollections(Collection<Map<String, Object>> collections) throws IOException, SolrServerException {
		SolrClient instance = null;
		try {
			instance = builder.build();
			Collection<SolrInputDocument> docs = anyRawCollectionsToDocuments(collections);
			while(!docs.isEmpty()) {
				Stream<SolrInputDocument> docsStream = docs.stream();
				Collection<SolrInputDocument> partCollections = docsStream.limit(100).collect(Collectors.toCollection(ArrayList::new));
				instance.add(partCollections);
				instance.commit();
				docs.removeAll(partCollections);
			}
			return true;
		} catch (SolrServerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			instance.rollback();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			instance.rollback();
		} finally {
			if (null != instance) {
				instance.close();
			}
		}
		return false;
	}

	/**
	 * Get list of documents.
	 * @param query
	 * @return
	 * @throws SolrServerException
	 * @throws IOException
	 */
	public SolrDocumentList query(SolrQuery query) throws SolrServerException, IOException {
		SolrClient instance = null;
		try {
			instance = builder.build();
			QueryResponse response = instance.query(query);
			return response.getResults();
		}  finally {
			if (null != instance) {
				instance.close();
			}
		}
	}
}


