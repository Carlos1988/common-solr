package knowledge.base.common.library.solr;

public class SolrConnectionParameters {
	private String domain;
	private String port;
	private ISolrDocument document;
	private String url;
	
	public SolrConnectionParameters(boolean isHttps, String domain, String port, ISolrDocument document) {
		this.domain = domain;
		this.port = port;
		this.document = document;
		this.url = new StringBuilder("http").append(isHttps ? "s" : "").append("://")
				.append(domain).append(":")
				.append(port.isEmpty() ? "" : port)
				.append("/solr/")
				.append(document.getName()).toString();
	}
	
	public String getDocumentName(){
		return this.document.getName();
	}
	
	public String getPort(){
		return this.port;
	}
	
	public String getDomain() {
		return this.domain;
	}
	
	public String getUrl(){
		return this.url;
	}
}
