package knowledge.base.common.library.solr;

import org.apache.solr.client.solrj.impl.HttpSolrClient;

public final class SolrConnectionFactory {
	private SolrConnectionFactory(){}
	/**
	 * Create a solr connection given set of parameters.
	 * @return
	 */
	public static SolrConnection getConnection(SolrConnectionParameters solrParams) {
		return new SolrConnection(new HttpSolrClient.Builder(solrParams.getUrl()));
	}
}
